// Photo viewer 2

var index = 0;
var c;
var ctx;
var imgs;
var maxindex;
var temp;

window.onload = function () {
    imgs = document.getElementById("images").children;
    maxindex = document.getElementById("images").children.length - 1;
    temp = document.getElementsByTagName("imgsrc");
    drawImg();
};

function nextImg() {
    if (index < maxindex) {
        index++;
    }
    else {
        index = 0;
    }
    drawImg();
}

function prevImg() {
    if (index > 0) {
        index--;
    }
    else {
        index = maxindex;
    }
    drawImg();
}

function drawImg() {
    var imageHolder = document.getElementById("pv_img_holder");
    imageHolder.setAttribute("src", "pictures/" + imgs[index].innerHTML);
}

function hover(id) {
    var element = document.getElementById(id);
    if (element.getAttribute("src") == "pictures/prev_img.png") {
        element.setAttribute("src", "pictures/prev_img2.png");
    }
    else if (element.getAttribute("src") == "pictures/next_img.png") {
        element.setAttribute("src", "pictures/next_img2.png");
    }
}

function unhover(id) {
    var element = document.getElementById(id);
    if (element.getAttribute("src") == "pictures/prev_img2.png") {
        element.setAttribute("src", "pictures/prev_img.png");
    }
    else if (element.getAttribute("src") == "pictures/next_img2.png") {
        element.setAttribute("src", "pictures/next_img.png");
    }
}

function generatePV(id, width, heigth, srcs) {
    srcs_t = srcs.split(",");

    var div = document.getElementById(id);

    div.setAttribute("id", "pv_div");
    var ul = document.createElement("ul");
    ul.setAttribute("id", "images");
    div.appendChild(ul);

    for (i = 0; i < srcs_t.length; i++) {
        var li = document.createElement("li");
        li.innerHTML = srcs_t[i];
        ul.appendChild(li);
    }
    var img = document.createElement("img");
    img.setAttribute("id", "pv_img_holder");
    div.appendChild(img);

    var br = document.createElement("br");
    div.appendChild(br);

    var div2 = document.createElement("div");
    div2.setAttribute("id", "pv_buttons");
    div.appendChild(div2);

    img = document.createElement("img");
    img.setAttribute("id", "pv_button1");
    img.setAttribute("class", "pv_button");
    img.setAttribute("src", "pictures/prev_img.png");
    img.setAttribute("onclick", "prevImg()");
    img.setAttribute("onmouseover", "hover('pv_button1')");
    img.setAttribute("onmouseout", "unhover('pv_button1')");
    div2.appendChild(img);

    img = document.createElement("img");
    img.setAttribute("id", "pv_button2");
    img.setAttribute("class", "pv_button");
    img.setAttribute("src", "pictures/next_img.png");
    img.setAttribute("onclick", "nextImg()");
    img.setAttribute("onmouseover", "hover('pv_button2')");
    img.setAttribute("onmouseout", "unhover('pv_button2')");
    div2.appendChild(img);

    /*
    <div id="pv_div">
        <ul id="images">
            <li>Logo.png</li>
            <li>sajt.bmp</li>
        </ul>
        <img id="pv_img_holder" /><br />
        <div id="pv_buttons">
            <img id="pv_button1" class="pv_button" src="pictures/prev_img.png" onclick="prevImg()" onmouseover="hover('pv_button1')" onmouseout="unhover('pv_button1')" />
            <img id="pv_button2" class="pv_button" src="pictures/next_img.png" onclick="nextImg()" onmouseover="hover('pv_button2')" onmouseout="unhover('pv_button2')" />
        </div>
    </div>
    */
}