if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
    window.onscroll = function () {
        var menu_style = document.getElementsByTagName("nav")[0].style;
        var header_offsetH = document.getElementsByTagName("header")[0].offsetHeight;
        if (window.pageYOffset >= header_offsetH && menu_style.position == "unset") {
            menu_style.position = "fixed";
            menu_style.top = "0px";
        }
        else if (!(window.pageYOffset >= header_offsetH)) {
            menu_style.position = "unset";
        }
    };
}