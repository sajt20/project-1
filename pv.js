//photo viewer

var imgs;
var currentIndex = 0;
var c;
var ctx;

window.onload = function () {
    imgs = document.getElementsByClassName("pv_imgs");
    c = document.getElementById("pv_c");
    ctx = c.getContext("2d");
    drawImg();
};

function nextImg() {
    if (currentIndex < --imgs.length) {
        currentIndex++;
    }
    else {
        currentIndex = 0;
    }
    drawImg();
}

function prevImg() {
    if (currentIndex > 0) {
        currentIndex--;
    }
    else {
        currentIndex = --imgs.length;
    }
    drawImg();
}

function drawImg() {
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.drawImage(imgs[currentIndex], 0, 0, c.width, c.height);
}

function hover(id) {
    var element = document.getElementById(id);
    if (element.getAttribute("src") == "pictures/prev_img.png") {
        element.setAttribute("src", "pictures/prev_img2.png");
    }
    else if (element.getAttribute("src") == "pictures/next_img.png") {
        element.setAttribute("src", "pictures/next_img2.png");
    }
}

function unhover(id) {
    var element = document.getElementById(id);
    if (element.getAttribute("src") == "pictures/prev_img2.png") {
        element.setAttribute("src", "pictures/prev_img.png");
    }
    else if (element.getAttribute("src") == "pictures/next_img2.png") {
        element.setAttribute("src", "pictures/next_img.png");
    }
}