
function getNewTipp() {
    if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        var index;

        var xhttp;
        if (window.XMLHttpRequest) {
            xhttp = new XMLHttpRequest();
        }
        else {
            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("tipp_div").innerHTML = "Tipp: <br />" + this.responseText;
            }
        };

        index = Math.floor(Math.random() * (4 - 1)) + 1;
        xhttp.open("GET", ("tipps/tipp" + index + ".txt"), true);
        xhttp.send();

        setInterval(function () {
            index = Math.floor(Math.random() * (4 - 1)) + 1;
            xhttp.open("GET", ("tipps/tipp" + index + ".txt"), true);
            xhttp.send();
        }, 10000);
    }
}

function hide() {
    var tipp_div_style = document.getElementById("tipp_div").style;
    if (tipp_div_style.left != "-210px") {
        tipp_div_style.animation = "hide 1s linear 0s alternate";
        tipp_div_style.left = "-210px";
    }
    else {
        tipp_div_style.animation = "show 1s linear 0s alternate";
        tipp_div_style.left = "0px";
    }
}